import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WashingMachineDatabase {

	List<WashingMachine> washingMachinesList = new ArrayList<>();
	
	public void addToList(WashingMachine washingMachine) {
		washingMachinesList.add(washingMachine);
	}
	
	public List<WashingMachine> getList(){
		return washingMachinesList;
	}
	
	private List<WashingMachine> getSortedList(List<WashingMachine> washingMachinesList){
		return sortList(washingMachinesList);
	}
	
	private List<WashingMachine> sortList(List<WashingMachine> washingMachinesList){
		return washingMachinesList.stream()
				.sorted((x, y) -> (x.getProducer().getProducerName().compareTo(y.getProducer().getProducerName())))
				.collect(Collectors.toList());
	}
	
	public void printWashingMachinesList() {
		getList().stream().forEach(System.out::println);
	}
	
	public void printSortedWashingMachinesList() {
		getSortedList(washingMachinesList).stream().forEach(System.out::println);
	}
	
}
