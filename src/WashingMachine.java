
public abstract class WashingMachine {

	private double temp = 0;
	private int program = 1;
	private int spinningV = 0;
	private Producers producer;
	private final int MAX_PROGRAMS;
	private final float MAX_TEMP_STEP;
	
	public WashingMachine(int maxPrograms, float maxTempStep) {
		this.MAX_PROGRAMS = maxPrograms;
		this.MAX_TEMP_STEP = maxTempStep;
	}
	
	public void setTemp(double temp) {
		double newTemp = roundTemp(temp);
		try {
			this.temp = checkIfTempInRange(newTemp);
		} catch (TempOutOfRangeException e) {
			e.printStackTrace();
		}
	};
	
	public double getTemp() {
		return temp;
	}
	
	public void tempUp() {
		if(temp < 90) {
			setTemp(getTemp() + MAX_TEMP_STEP);
		} else {
			System.out.println("Nie można bardziej zwiększyć temperatury");
		}
	}
	
	public void tempDown() {
		if(temp > 0) {
			setTemp(getTemp() - MAX_TEMP_STEP);
		} else {
			System.out.println("Nie można bardziej zmniejszyć temperatury");
		}
	}
	
	public void setProgram(int programNumber) {
		try {
			program = checkIfProgramInRange(programNumber);
		} catch (ProgramOutOfRangeException e) {
			e.printStackTrace();
		}
	}
	
	public int getProgram() {
		return program;
	}
	
	public void nextProgram() {
		setProgram(getProgram() +1);
	}
	
	public void previousProgram() {
		setProgram(getProgram() -1);
	}
	
	public void setV(int speed) {
		int roundedSpeed = roundToHundreds(speed);
		if(roundedSpeed >= 0 && roundedSpeed <= 1000) {
			spinningV = roundedSpeed;
		}
	}
	
	public int getV() {
		return spinningV;
	}
	
	public void upV() {
		int futureSpeed = getV() + 100;
		if(futureSpeed > 1000) {
			spinningV = 0;
		} else {
			spinningV = futureSpeed;
		}
	}
	
	public void downV() {
		int futureSpeed = getV() - 100;
		if(futureSpeed < 0) {
			spinningV = 1000;
		} else {
			spinningV = futureSpeed;
		}
	}
	
	public void showStatus() {
		System.out.println("Pralka typu " + producer.getProducerName() + ": \n" + "program -> " + program + " \nprędkość wirowania -> " + spinningV + "\ntemperatura -> " + temp + " \u00b0C\n");
	}

	public Producers getProducer() {
		return producer;
	}

	public void setProducer(Producers producer) {
		this.producer = producer;
	}
	
	/**
	 * Checks if the fiven temperature is within the limitations (between 0 and 90)
	 * @param temp Temperature value to validate
	 * @return The given temperature if passes validation or 0 if given temperature was out of range
	 * @throws TempOutOfRangeException Throws error when the given temperature is out of available range
	 */
	protected double checkIfTempInRange(double temp) {
		double tempToCheck = temp;
		if(tempToCheck > 90 || tempToCheck < 0) {
			tempToCheck = 0;
			throw new TempOutOfRangeException("Given temperature is out of available range!");
		} 
		printTemp(tempToCheck);
		return tempToCheck;
	}
	
	/**
	 * Checks if the given program is within the limitations (between 0 and maxPrograms).
	 * @param program Program number to validate
	 * @return The given value if passes validation or the number of the program that was set up previously
	 * @throws ProgramOutOfRangeException Throws error when the given program is out of the available range
	 */
	protected int checkIfProgramInRange(int program) {
		int programToCheck = program;
		if(programToCheck < 1 || programToCheck > MAX_PROGRAMS) {
			programToCheck = this.getProgram();
			throw new ProgramOutOfRangeException("Given program is out of available range!");
		}
		return programToCheck;
	}
	
	private int roundToHundreds(int number) {
		return (int)(Math.round(((double)number / 100))*100);
	}
	
	public void printTemp(double temp) {
		System.out.println("Temperatura ustawiona na " + temp + " \u00b0C");
	}
	
	/** 
	 * Round the given temperature to the closest available temperature step in accordance with the maxTempStep.
	 */
	public double roundTemp(double temp) {
		return Math.round(temp * (1/MAX_TEMP_STEP)) / (1/MAX_TEMP_STEP);
	}

	@Override
	public String toString() {
		return "WashingMachine [temp=" + temp + ", program=" + program + ", spinningV=" + spinningV + ", producer="
				+ producer + ", maxPrograms=" + MAX_PROGRAMS + ", maxTempStep=" + MAX_TEMP_STEP + "]";
	}
}
