
public class Main {

	public static void main(String[] args) {


		Beko beko = new Beko();

		Amica amica = new Amica();
	
		Whirlpool whirlpool = new Whirlpool();


		
		System.out.println(" ----  Beko  ----");
		
		System.out.println("temp: " + beko.getTemp());

		beko.setTemp(89.3);
		System.out.println("setTemp(): " + beko.getTemp());
		
		beko.tempUp();
		System.out.println("tempUp(): " + beko.getTemp());
				
		System.out.println("tempUp() poza zakres : " + beko.getTemp());
		beko.tempUp();
		
		beko.tempDown();
		System.out.println("tempDown(): " + beko.getTemp());
		
		System.out.println("setTemp() poza zakres : " );
		beko.setTemp(100);
		
		System.out.println("");
		beko.showStatus();
		System.out.println("");
		
		System.out.println(" ----  Amica  ----");
		
		System.out.println(" getV() : " + amica.getV());
		
		amica.setV(900);
		
		System.out.println(" getV() : " + amica.getV());
		
		amica.upV();
		System.out.println(" upV() : " + amica.getV());
		
		amica.upV();
		System.out.println(" upV() poza zakres : " + amica.getV());
		
		amica.downV();
		System.out.println(" downV() : " + amica.getV());
		
		System.out.println("");
		amica.showStatus();
		System.out.println("");
		
		System.out.println(" ----  Wihrlpool  ----");
		
		System.out.println("getProgram() : " + whirlpool.getProgram());
		
		whirlpool.setProgram(24);
		System.out.println("getProgram() : " + whirlpool.getProgram());

		whirlpool.nextProgram();
		System.out.println("nextProgram() : " + whirlpool.getProgram());
		
		whirlpool.nextProgram();
		System.out.println("nextProgram() poza zakres : " + whirlpool.getProgram());
		
		whirlpool.previousProgram();
		System.out.println("previousProgram() : " + whirlpool.getProgram());
		
		System.out.println("");
		whirlpool.showStatus();
		System.out.println("");
		
		System.out.println(" ----  List  ----");
		
		WashingMachineDatabase database = new WashingMachineDatabase();
		
		database.addToList(beko);
		database.addToList(whirlpool);
		database.addToList(amica);
		
		System.out.println("not sorted :");
		database.printWashingMachinesList();
		
		System.out.println("sorted :");
		database.printSortedWashingMachinesList();
	}
}
