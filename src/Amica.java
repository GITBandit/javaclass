
public class Amica extends WashingMachine {

	public Amica() {
		super(Producers.AMICA.getMaxProgram(), Producers.AMICA.getMaxTempStep());
		this.setProducer(Producers.AMICA);
	}

	
}
