/**
 * Enum contains washing machines' producers with names and the limitations such as maximal program number and temperature step for each washing machine.
*/
public enum Producers {

	BEKO("Beko", 1f), 
	WHIRLPOOL("Whirlpool",25), 
	AMICA("Amica");
	
	String producerName;
	float maxTempStep;
	int maxProgram;
	
	Producers(String producerName) {
		this.producerName = producerName;
		this.maxTempStep = 0.5f;
		this.maxProgram = 20;
	}
	
	Producers(String producerName, float maxTempStep) {
		this.producerName = producerName;
		this.maxTempStep = maxTempStep;
		this.maxProgram = 20;
	}
	
	Producers(String producerName, int maxProgram) {
		this.producerName = producerName;
		this.maxTempStep = 0.5f;
		this.maxProgram = maxProgram;
	}
	
	Producers(String producerName, float maxTempStep, int maxProgram) {
		this.producerName = producerName;
		this.maxTempStep = maxTempStep;
		this.maxProgram = maxProgram;
	}
	
	public String getProducerName() {
		return producerName;
	}
	
	public float getMaxTempStep() {
		return maxTempStep;
	}
	
	public int getMaxProgram() {
		return maxProgram;
	}
	
}
