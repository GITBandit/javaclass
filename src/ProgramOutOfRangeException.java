
public class ProgramOutOfRangeException extends RuntimeException {

	public ProgramOutOfRangeException(String message){
		super(message);
	}
}
