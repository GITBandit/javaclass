
public class Beko extends WashingMachine {
	
	public Beko() {
		super(Producers.BEKO.getMaxProgram(), Producers.BEKO.getMaxTempStep());
		this.setProducer(Producers.BEKO);
	}
}
