
public class Whirlpool extends WashingMachine {

	public Whirlpool() {
		super(Producers.WHIRLPOOL.getMaxProgram(), Producers.WHIRLPOOL.getMaxTempStep());
		this.setProducer(Producers.WHIRLPOOL);
	}

}
