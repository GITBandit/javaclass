
public class TempOutOfRangeException extends RuntimeException {

	public TempOutOfRangeException(String message) {
		super(message);
	}
}
